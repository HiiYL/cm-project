function y = RED(m,n)
    if(isset('n'))
        seed = rand(m,n);
    else
        seed = rand(m,m);
    end
    
    a = 0;
    b = 1;
    y = (a + (b-a)*seed);
    end
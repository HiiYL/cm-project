function y = RED(m,n)
    if(isset('n'))
        seed = rand(m,n);
    else
        seed = rand(m,m);
    end
    lambda = rand();
    y = mod(((-1/lambda)*log(1-seed)),1);
    end
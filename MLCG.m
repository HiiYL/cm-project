function y = MLCG(m,n)
    if(isset('n'))
        seed = rand(m,n);
    else
        seed = rand(m,m);
    end
    a = 1103515245;
    c = 0;
    y = mod((a*seed + c),1);
    end
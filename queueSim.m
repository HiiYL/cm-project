function y = queueSim(randFunc, customer_count, servers_count)
    interarrival_random_numbers = ceil(randFunc(1,customer_count-1)*100);
    servers_random_numbers = ceil(randFunc(1,customer_count)*100); 
    random_number_range_to_interarrival_time = [ 25,15,30,15,10,5;1,2,3,4,5,6]; 
    random_number_range_to_service_time_default = [ 10,20,30,25,10,5;12,13,14,15,16,17];
                     %|  Probability   | Service Time |
    RNRTST(:,:,1) = [ 10,20,30,25,10,5;6,7,8,9,10,11];
    RNRTST(:,:,2) = [ 15,20,25,20,15,5;7,8,9,10,11,12];                 %DEFINES DIFFERENT RN TO SERVICE TIME FOR DIFFERENT SERVERS
    RNRTST(:,:,3) = [ 20,15,20,25,10,10;4,5,6,7,8,9];                 %YOU CAN ADD MORE BY FOLLOWING THE SYNAX RNRTSTD(:,:,N) = [ %,%,%,%,%,%,%,%,..; M,M,M,M,M,M,...]
    RNRTST(:,:,4) = [ 25,15,10,25,20,20;8,9,10,11,12,13];                 % N-SERVER COUNT          %- RANDOM NUMBERS              M-RESULTING SERVICE_TIME
    for i = size(RNRTST,3):servers_count                            %Generates tables for all non-customized servers
        RNRTST(:,:,i) = random_number_range_to_service_time_default;
    end
    
    for i = 1:customer_count-1                                          %Generates Interarrival Matrix
        for j = 1:size(random_number_range_to_interarrival_time,2)
                if(interarrival_random_numbers(i) < sum(random_number_range_to_interarrival_time(1,1:j)))
                    interarrival_time(i) = random_number_range_to_interarrival_time(2,j);
                break;
            end
        end
    end
    arrival_time = 0;
    for i = 1:customer_count-1
        arrival_time(i+1) = sum(interarrival_time(1, 1:i));
    end
    for i = 1:servers_count                                                    %Generates service time matrix                 
      for j = 1:customer_count                       
          for k = 1:size(RNRTST,2)
              if(servers_random_numbers(j) < sum(RNRTST(1,1:k,i)))
                  service_time(i,j) = RNRTST(2,k,i);
                  break;
              end
          end
      end
    end
    server_departure_time = zeros(1,servers_count);  
    for i = 1:customer_count                                                     %Calculates begin and end service time
        if arrival_time(i) > min(server_departure_time)
            begin_service_time(i) = arrival_time(i);
        else
            begin_service_time(i) = min(server_departure_time);
        end   
        server_index = find(server_departure_time== min(server_departure_time))(1);
        curr_service_time = service_time(server_index,i);
        departure_time(i) = begin_service_time(i) + curr_service_time;
        server_departure_time(server_index) = departure_time(i);
        customer_server(i) = server_index;
    end
    
    time_spent_in_system = departure_time-arrival_time;                               %calculates stats
    time_in_queue = begin_service_time - arrival_time;
    waiting_time = begin_service_time-arrival_time;
    avg_waiting_time = sum(waiting_time)/length(waiting_time);
    wait_probability = sum(begin_service_time > arrival_time)/customer_count;
          
    %Displays Interarrival time table
    disp('---------------------------------------------------------------------------------------------------------------------------')
    disp('INTERARRIVAL TIME  |   PROBABILITY  |   CDF   |  RANGE     ')
    disp('---------------------------------------------------------------------------------------------------------------------------')
    for i = 1:size(random_number_range_to_interarrival_time,2)
        fprintf('%17d  |    %10.2f  |  %.3f  |  %3d-%3d  \n',random_number_range_to_interarrival_time(2,i),random_number_range_to_interarrival_time(1,i)/100,sum(random_number_range_to_interarrival_time(1,1:i))/100, sum(random_number_range_to_interarrival_time(1,1:i-1))+1, sum(random_number_range_to_interarrival_time(1,1:i)));
    end    
    disp('---------------------------------------------------------------------------------------------------------------------------')
    
    %Displays Service time table
    for j = 1:servers_count
        fprintf('\n')
        disp('---------------------------------------------------------------------------------------------------------------------------')
        fprintf('SERVER %d \n', j);
        disp('---------------------------------------------------------------------------------------------------------------------------')
        disp('SERIVCE TIME  |   PROBABILITY  |   CDF   |  RANGE     ')
          for i = 1:size(RNRTST,2)
              fprintf('%12d  |    %10.2f  |  %.3f  |  %3d-%3d  \n',RNRTST(2,i,j),RNRTST(1,i,j)/100,sum(RNRTST(1,1:i,j))/100, sum(RNRTST(1,1:i-1,j))+1, sum(RNRTST(1,1:i,j)));
          end
        disp('---------------------------------------------------------------------------------------------------------------------------')   
    end 
    
    %Display Summary
    disp('---------------------------------------------------------------------------------------------------------------------')
    disp('                                                      SUMMARY')
    disp('---------------------------------------------------------------------------------------------------------------------')
    for i = 1:customer_count
        fprintf('Arrival of Customer %d at minute %d and queue at the counter %d\n', i, arrival_time(i), customer_server(i));
        fprintf('Service for customer %d begin at minute %d\n', i, begin_service_time(i)); 
        fprintf('Departure of Customer %d at minute %d\n', i, departure_time(i));
    end
    disp('---------------------------------------------------------------------------------------------------------------------')
    disp('---------------------------------------------------------------------------------------------------------------------')
    disp('Cust | RN       |  Interarrivals | Arrival | RN      | Time Service | Time Service | Time Spent | Time In |   Server')
    disp('No   | Arrivals |  Time          | Time    | Service | Begins       | Ends         | In System  | Queue   |       ')
    disp('---------------------------------------------------------------------------------------------------------------------')
    for i = 1:customer_count
        if i == 1
            fprintf('%4d | %9c | %14d | %7d | %7d | %12d | %12d | %10d | %7d | %8d\n',1,' ', 0, arrival_time(1), servers_random_numbers(1), begin_service_time(1),departure_time(1), time_spent_in_system(1), time_in_queue(1), customer_server(i));
        else
            fprintf('%4d | %8d | %14d | %7d | %7d | %12d | %12d | %10d | %7d | %8d\n',i, interarrival_random_numbers(i-1),  interarrival_time(i-1), arrival_time(i),servers_random_numbers(i),begin_service_time(i),departure_time(i), time_spent_in_system(i), time_in_queue(i), customer_server(i))
        end
    end
    disp('---------------------------------------------------------------------------------------------------------------------')
    
    %Display Stats
    fprintf('Average Waiting time is %.2f minutes \n', avg_waiting_time);
    fprintf('Probability that customer has to wait %.2f\n\n', wait_probability);  
    for i = 1:servers_count
        probability = sum(customer_server==i)/size(customer_server,2);
        served_customer_index = find(customer_server==i);
        number_of_served_customers = size(served_customer_index,2);
        total_time = sum(service_time(served_customer_index));
        fprintf('Server %d served %d customer(s) with a total service time of %d\n', i, number_of_served_customers, total_time);
        fprintf('Probability Server %d is busy: %f\n', i, probability);
    end
    for i = 1:servers_count
        server_customer(i) = sum(customer_server==i);
    end
    best_server = find(server_customer==max(server_customer));
    if(size(best_server,2) < 2)
        fprintf('Server %d serves the most customers!\n', best_server)
    end
 end
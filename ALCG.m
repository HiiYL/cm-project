function y = ALCG(m,n)
    if(isset('n'))
        seed = rand(m,n);
    else
        seed = rand(m,m);
    end
    a = 1;
    c = 12345;
    y = mod((a*seed + c),1);
    end
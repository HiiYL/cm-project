#Queue Simulation System
A simple queue simulator using Freemat
###To Use
* Open project folder in Freemat
* Use following command to simulate a queue

```
queueSim(@randFunc, customer_count, server_count)
```
Replace randFunc with a random number generator of your choice!

Available generators are:

1. rand (Built in Freemat random function)
2. LCG  (Linear Congruential Genereator)
3. MLCG (Multiplicative Linear Congruential Genereator)
4. ALCG (Additive Congruential Genereator)
5. RED  (Random Variate Generator With Exponential Distribution)
6. RUD  (Random Variate Generator With Uniform Distribution)

E.g.
```
queueSim(@RED, 10, 3)
```

